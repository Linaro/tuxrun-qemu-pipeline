FROM registry.gitlab.com/linaro/tuxrun-qemu/ci AS builder
ARG DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3008
COPY . /qemu
RUN cd /qemu \
    && ./configure --prefix=/opt/qemu --enable-strip --target-list=arm-softmmu,aarch64-softmmu,i386-softmmu,m68k-softmmu,mips-softmmu,mips64-softmmu,mipsel-softmmu,mips64el-softmmu,ppc-softmmu,ppc64-softmmu,riscv32-softmmu,riscv64-softmmu,s390x-softmmu,sh4-softmmu,sparc64-softmmu,x86_64-softmmu\
    && make -j$(nproc) \
    && make install \
    && ldd $(ls /opt/qemu/bin/qemu-system-*) | awk '{print($3)}' | sed -e 's/^\///' | xargs dpkg -S | cut -d : -f 1 | sort -u | tr '\n' ' ' | tee /opt/qemu/qemu-list-deps

FROM debian:bookworm-backports
ARG DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3008
COPY --from=builder /opt/qemu/ /usr/
RUN apt update \
    && apt install -yy $(cat /usr/qemu-list-deps) \
    && apt autoremove -yy \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /usr/qemu-list-deps
